﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="WebClient.UserProfile" UnobtrusiveValidationMode="None"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Profile</title>
     <link rel="stylesheet" type="text/css" href="design/UserProfile.css" />
</head>
<body>
    <form id="UserProfile" runat="server">
        <header class="logo">
            <a href="index.aspx" class="logo">
                <span>Booking</span>
                System
            </a>
            <div class="logout">
                <asp:Button class="astext" ID="SignOut" OnClick="Sign_Out" runat="server" Text="Log Out" />
            </div>
        </header>
        <nav></nav>
        <div id="wrapper">
            <h2>Your Profile</h2>
            
            <div class="updateForm">
               <div class="UpdateUsername">
        <asp:Label runat="server" AssociatedControlID="UpdateUserName">New User Name: </asp:Label>           
                 <div>    
            <asp:TextBox ID="UpdateUserName" runat="server"></asp:TextBox> 
             <asp:RequiredFieldValidator
                ID="RequiredFieldValidatoUserName" 
                ControlToValidate="UpdateUserName"
                Display="Static"
                runat="server"
                ErrorMessage="Please enter your username">
                </asp:RequiredFieldValidator>
                </div>   
        </div>                   
        <br />
        <div class="UpdateName">
        <asp:Label runat="server" AssociatedControlID="UpdateName">New Name: </asp:Label>
            <div>
                <asp:TextBox ID="UpdateName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorName"
                ControlToValidate="UpdateName"
                Display="Static"
                runat="server"
                ErrorMessage="Please enter your name">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <br />
        <div class="UpdateCPR">
            <asp:Label runat="server" AssociatedControlID="UpdateCPR">New CPR Number: </asp:Label>
          <div>
            <asp:TextBox ID="UpdateCPR" runat="server" PlaceHolder="123456-1234"></asp:TextBox> 
            <asp:RegularExpressionValidator
                ID="CprNumberValidator"
                runat="server"
                ControlToValidate="UpdateCPR"
                Display="Static"
                ErrorMessage="CPR number form must be 123456-1234"
                ValidationExpression="^\d{6}-\d{4}$">
            </asp:RegularExpressionValidator>
          </div>
    </div>
        <br />
        <div class="UpdateEmail">
        <asp:Label runat="server" AssociatedControlID="UpdateEmail">New Email: </asp:Label>
            <div>
            <asp:TextBox ID="UpdateEmail" runat="server" PlaceHolder="someone@example.com"></asp:TextBox> 
                <asp:RegularExpressionValidator 
                ID="emailValidator"
                ControlToValidate="UpdateEmail"
                Display="Static"
                runat="server" 
                ErrorMessage="Example: someone@example.com"
                ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <br />
        <div class="UpdatePhone">
            <asp:Label runat="server" AssociatedControlID="UpdatePhone">New Phone: </asp:Label>
            <div>
            <asp:TextBox ID="UpdatePhone" runat="server"></asp:TextBox> 
                <asp:RegularExpressionValidator 
                ID="phoneValidator"
                ControlToValidate="UpdatePhone"
                 Display="Static"
                runat="server" 
                ErrorMessage="Phone number form must be +4512345678"
                 ValidationExpression="^((\(?\+45\)?)?)(\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$">
            </asp:RegularExpressionValidator>
            </div>
        </div>
        <br />
            </div>

            <div class="deleteAcc">

                    <asp:Label runat="server" AssociatedControlID="DeleteAccount">Delete Account:</asp:Label>
                    <div>
                    <asp:TextBox ID="DeleteAccount" runat="server" PlaceHolder="Enter CPR Number"></asp:TextBox>
                    <asp:Button ID="Delete" OnClick="Delete_Account" runat="server" Text="Delete" />
                    </div>
             
            </div>
            <div class="update">
                <asp:Button ID="Update" OnClick="UpdateCustomer" runat="server" Text="Update" />
            </div>

        </div>
    </form>
</body>
</html>
