﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebClient.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Login</title>
     <link rel="stylesheet" type="text/css" href="design/login.css" />
</head>
<body>
   <form id="login" runat="server">
       <header>
        <a href="index.aspx" class="logo">
            <span>Booking</span>
                  System
        </a>
           <div class="Register">
                <asp:Button class="astext" ID="Registration" OnClick="Register" runat="server" Text="Register" />
            </div>
       </header>
      <div id="wrapper">
          <div class="LoginForm">
              <span class="signin">Sign in</span>
          <asp:PlaceHolder runat="server" ID="LoginStatus" Visible="false">
            <p>
               <asp:Literal runat="server" ID="StatusText" />
            </p>
         </asp:PlaceHolder>
       <asp:PlaceHolder runat="server" ID="LoginForm" Visible="true">
           <div class="loginfields">
            <div>
               <asp:Label runat="server" AssociatedControlID="UserName">Username</asp:Label>
                  <asp:TextBox runat="server" ID="UserName" /> 
            </div>
            <div>
               <asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label>
                   <asp:TextBox runat="server" ID="Password" TextMode="Password" />
          </div>
               </div>
       </asp:PlaceHolder>
       <div class="signin">
            <asp:Button ID="signin" OnClick="SignIn" runat="server" Text="Sign In" />
        </div>
              <p>Don`t you have an account yet? <br /> <a href="Registration.aspx">Register Now!</a></p>
              </div>
          </div>
   </form>
</body>
</html>
