﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebClient.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home Page</title>
     <link rel="stylesheet" type="text/css" href="design/index.css" />
</head>
<body>
    <form id="index" runat="server">
       <header class="logo">
            <a href="index.aspx" class="logo">
                <span>Booking</span>
                System
            </a>
            <div class="login">
                <asp:Button class="astext" ID="LogIn" OnClick="Log_in" runat="server" Text="Register / Log in" />
            </div>
        </header>
    <nav>
        <div class="bookapp">
        <a href="Calendar.aspx"><img src="images/BookApp.png"/></a>
        </div>
        <div class="myapp">
        <a href="Appointments.aspx"><img src="images/MyApp.png"/></a>
        </div>
        
        <div class="UserProfile">
        <a href="UserProfile.aspx"><img src="images/MyProf.png" /></a>
        </div>
        <div class="cmg">
        <img src="images/cmg.png" />
        </div>
    </nav>
    <div>
    
    </div>
    </form>
</body>
</html>
