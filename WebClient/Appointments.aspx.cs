﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebClient.ServiceReference1;
using System.Text;
using Microsoft.AspNet.Identity;
using WcfService.Model;

namespace WebClient
{
    public partial class Appointments : System.Web.UI.Page
    {
        DataTable myApp = new DataTable();
        Appointment[] list;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) GetData();
        }

        protected void GetData()
        {
            var client = new RegisterServiceClient();
            var username = HttpContext.Current.User.Identity.GetUserName();          
            
            list = client.GetAllCustomersAppointmentsforUserName(username);
        }

        protected string getShowHtml()
        {
            StringBuilder html = new StringBuilder();
            if (User.Identity.IsAuthenticated)
            {
                
                var client = new RegisterServiceClient();

                foreach (var c in list)
                {

                    Job j = client.findJob(c.job.id);


                    html.Append(String.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>", c.date, c.start_time, j.name, j.duration, c.notes));
                }
            }
            else
            {
                Server.Transfer("Login.aspx");
            }
                return html.ToString();
            
            
            
        }
        protected string getMyApp()
        {
            StringBuilder html = new StringBuilder();
            foreach (DataRow row in myApp.Rows)
            {
                html.Append("<tr>");
                foreach (DataColumn column in myApp.Columns)
                {
                    html.Append("<td>");
                    html.Append(row[column.ColumnName]);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }
            return html.ToString();

        }
        protected void Sign_Out(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/index.aspx");
        }
    }
}