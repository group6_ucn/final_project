﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="WebClient.Registration" UnobtrusiveValidationMode="None"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration</title>
    <link rel="stylesheet" type="text/css" href="design/registration.css" />
</head>
<body>
    <form id="RegistrationForm" runat="server">
            <header>
                <a href="index.aspx" class="logo">
                <span>Booking</span>
                System
                </a>
            </header>
    <div id="wrapper">
        <h2>Registration - please fill up the form</h2>
        <p>
            <asp:Literal runat="server" ID="StatusMessage" />
        </p>
        <div class="registrationForm">
        <div class="username">
        <asp:Label runat="server" AssociatedControlID="UserName">User Name: </asp:Label>           
                 <div>    
            <asp:TextBox ID="UserName" runat="server"></asp:TextBox> 
             <asp:RequiredFieldValidator
                ID="RequiredFieldValidatoUserName" 
                ControlToValidate="UserName"
                Display="Static"
                runat="server"
                ErrorMessage="Please enter your username">
                </asp:RequiredFieldValidator>
                </div>   
        </div>                   
        <br />
        <div class="Name">
        <asp:Label runat="server" AssociatedControlID="Name">Your Name: </asp:Label>
            <div>
                <asp:TextBox ID="Name" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorName"
                ControlToValidate="Name"
                Display="Static"
                runat="server"
                ErrorMessage="Please enter your name">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <br />
        <div class="CPR">
            <asp:Label runat="server" AssociatedControlID="CPR">Your CPR Number: </asp:Label>
          <div>
            <asp:TextBox ID="CPR" runat="server"></asp:TextBox> 
            <asp:RegularExpressionValidator
                ID="CprNumberValidator"
                runat="server"
                ControlToValidate="CPR"
                Display="Static"
                ErrorMessage="CPR number form must be 123456-1234"
                ValidationExpression="^\d{6}-\d{4}$">
            </asp:RegularExpressionValidator>
          </div>
    </div>
        <br />
        <div class="Email">
        <asp:Label runat="server" AssociatedControlID="Email">Your Email: </asp:Label>
            <div>
            <asp:TextBox ID="Email" runat="server"></asp:TextBox> 
                <asp:RegularExpressionValidator 
                ID="emailValidator"
                ControlToValidate="Email"
                Display="Static"
                runat="server" 
                ErrorMessage="Example: someone@example.com"
                ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <br />
        <div class="Phone">
            <asp:Label runat="server" AssociatedControlID="Phone">Your Phone: </asp:Label>
            <div>
            <asp:TextBox ID="Phone" runat="server"></asp:TextBox> 
                <asp:RegularExpressionValidator 
                ID="phoneValidator"
                ControlToValidate="Phone"
                 Display="Static"
                runat="server" 
                ErrorMessage="Phone number form must be +4512345678"
                 ValidationExpression="^((\(?\+45\)?)?)(\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$">
            </asp:RegularExpressionValidator>
            </div>
        </div>
        <br />
        <div class="Password">
            <asp:Label runat="server" AssociatedControlID="Password">Your Password: </asp:Label>
            <div class="pw">
            <asp:TextBox ID="Password" TextMode="Password" runat="server"></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="ConfirmPassword">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword">Confirm Password: </asp:Label>
            <div class="pwconfirm">
            <asp:TextBox ID="ConfirmPassword" TextMode="Password" runat="server"></asp:TextBox>
            </div>

        </div>
            </div>
        <br />
         <div class="submit">
            <asp:Button ID="Submit" OnClick="Register_Click" runat="server" Text="Submit" />
        </div>
       </div>
    </form>
</body>
</html>
