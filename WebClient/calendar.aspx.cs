﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WcfService.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebClient.ServiceReference1;



namespace WebClient
{  
    public partial class Calendar : System.Web.UI.Page
    {
        Job[] lists;
        Company[] allComp;
        protected void Page_Load(object sender, EventArgs e)
        {
                   
            if (!IsPostBack)
            {
                fillCompList();
               
            }
            //if(IsPostBack)
            //{
            //    fillList(int companyCVR);
            //}
        }

        protected void selectJob_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            selectDate.Text = Calendar1.SelectedDate.ToShortDateString();
           
        }
        protected void customerCPR_TextChanged(object sender, EventArgs e)
        {

        }
        protected void Register_Click(object sender, EventArgs e)
        {
            
                var p = new RegisterServiceClient();
                Appointment c = new Appointment();
                c.customer = new Customer();
                c.company = new Company();
                c.job = new Job();
                Job f = new Job();
                f.name = Jobs.Text;
                f = p.findJobByName(f.name);
                Company y = new Company();
                y.name = Companies.Text;
                y = p.findCompanyByName(y.name);

                c.id = Int32.Parse(ID.Text);
                c.date = DateTime.Parse(selectDate.Text + dropdownhour.Text);
                c.customer.username = HttpContext.Current.User.Identity.GetUserName();
                c.company.CVR = y.CVR;
                c.notes = notes.Text;
                c.job.id = f.id;
                c.start_time = dropdownhour.Text;
                if (User.Identity.IsAuthenticated)
                {
                    p.SaveAppointment(c);
                    Response.Write("<script>alert('Your appointment has been reserved.')</script>");
                }
                else
                {
                    Server.Transfer("Login.aspx");
                }
            }

        protected void dropdownhour_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void selectCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            var client = new RegisterServiceClient();
            Company c = new Company();
            c.name = Companies.Text;
            c = client.findCompanyByName(c.name);
            Jobs.Items.Clear();
            fillList(c.CVR);
            
        }

        protected void fillCompList()
        {
            var client = new RegisterServiceClient();
            allComp = client.GetAllCompanies();
            DropDownList Companies = new DropDownList();
            foreach(var c in allComp)
            {
                 
                this.Companies.Items.Add(new ListItem(c.name.ToString()));
            }
        }
        protected void fillList(int companyCVR) 
        {  
            var client = new RegisterServiceClient();
            lists = client.GetAllJobs(companyCVR);
            DropDownList Jobs = new DropDownList();
            foreach (var j in lists)
            {
                
                this.Jobs.Items.Add(new ListItem(j.name.ToString()));
            }
        }
       

        protected void Sign_Out(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/index.aspx");
        }
    }
}