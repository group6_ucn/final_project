﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Appointments.aspx.cs" Inherits="WebClient.Appointments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Appointments</title>
    <link rel="stylesheet" type="text/css" href="design/Appointments.css" />
</head>
<body>
    <form id="appointments" runat="server">

        <header class="logo">
            <a href="index.aspx" class="logo">
                <span>Booking</span>
                System
            </a>
            <div class="logout">
                <asp:Button class="astext" ID="SignOut" OnClick ="Sign_Out" runat="server" Text="Log Out" />
            </div>
        </header>
        
        <nav>
            
        </nav>

    <div class="MyAppointments">
        <h3>Upcoming Appointments</h3>
        <table border="1">
        <thead>
            <tr><th>Date</th><th>Time</th><th>Job</th><th>Duration</th><th>Description</th></tr>
        </thead>
        <tbody>
               <%= getShowHtml() %>
        </tbody>
    </table>
    </div>
    </form>
</body>
</html>
