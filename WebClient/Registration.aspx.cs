﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WcfService.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebClient.ServiceReference1;

namespace WebClient
{
    public partial class Registration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Register_Click(object sender, EventArgs e)
        {   var p = new RegisterServiceClient();
            Customer c = new Customer();
           
            c.name = Name.Text;
            c.CPR = CPR.Text;
            c.email = Email.Text;
            c.phone = Phone.Text;
      
            c.username = UserName.Text;
            p.SaveCustomer(c);
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser()
            {
                UserName = UserName.Text
                
        
        };
            
            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);
            Response.Redirect("~/Login.aspx");
            }
            else
            {
                StatusMessage.Text = result.Errors.FirstOrDefault();
            }
           
            //var p = new RegisterServiceClient();
            //var c = new Customer();
            //c.name = Name.Text;
            //c.CPR = CPR.Text;
            //c.email = Email.Text;
            //c.phone = Phone.Text;
            //c.password = Password.Text.GetHashCode();
            //c.username = UserName.Text;
            //p.SaveCustomer(c);
            //if (!Page.IsValid)
            //    return;
            //Server.Transfer("TFRegistration.html");
        }
    }
}