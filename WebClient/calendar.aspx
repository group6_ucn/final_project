﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="WebClient.Calendar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calendar</title>
    <link rel="stylesheet" type="text/css" href="design/Calendar.css" />
  
</head>
    <body>
    <form id="form1" runat="server">

        <header class="logo">
            <a href="index.aspx" class="logo">
                <span>Booking</span>
                System
            </a>
            <div class="logout">
                <asp:Button class="astext" ID="SignOut" OnClick ="Sign_Out" runat="server" Text="Log Out" />
            </div>
        </header>
        
        <nav>
            
        </nav>
        <div class="CalendarForm">
    <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" style="z-index: 1; left: 339px; top: 120px; position: absolute; height: 188px; width: 259px; right: 253px;">
    </asp:Calendar>

        <div class="ID">
            <asp:Label runat="server">Your ID: </asp:Label>
          <div>
            <asp:TextBox ID="ID" runat="server"></asp:TextBox> 
          </div>
            </div>
            <br />

        <div class="selectDate">
            <asp:Label runat="server">Select date: </asp:Label>
        <div>
            <asp:TextBox ID="selectDate" runat="server"></asp:TextBox> 
            </div>
        </div>
            <br />
         
         <div class="Companies">
             <asp:Label runat="server">Select a Company: </asp:Label>
             <br />
              <asp:dropdownlist runat="server" ID="Companies"
                                               AutoPostBack="true"
                                               OnSelectedIndexChanged="selectCompany_SelectedIndexChanged"
                                               >
                  <asp:ListItem>Select a Company...</asp:ListItem>
              </asp:dropdownlist>
         </div>
             <br />
            
         <div class="notes">
            <asp:Label runat="server">Notes: </asp:Label>
         <div>
            <asp:TextBox ID="notes" runat="server"></asp:TextBox> 
         </div>
             </div>
             <br />

          <div class="Job">
              <asp:Label runat="server">Select a job: </asp:Label>
              <asp:dropdownlist runat="server" ID="Jobs"
                                               AutoPostBack="false"
                                               OnSelectedIndexChanged="selectJob_SelectedIndexChanged"
                                                                                             >
                  <asp:ListItem>Select a Job...</asp:ListItem>
              </asp:dropdownlist>
                                                            
          </div>                                
              <br />

              <div class ="Hour">
                  <asp:Label runat="server">Select hour: </asp:Label>
                  <asp:DropDownList ID = "dropdownhour" runat="server" OnSelectedIndexChanged="dropdownhour_SelectedIndexChanged">
                      <asp:ListItem> 08:00 </asp:ListItem>
                      <asp:ListItem> 09:00 </asp:ListItem>
                      <asp:ListItem> 10:00 </asp:ListItem>
                      <asp:ListItem> 11:00 </asp:ListItem>
                      <asp:ListItem> 12:00 </asp:ListItem>
                      <asp:ListItem> 13:00 </asp:ListItem>
                      <asp:ListItem> 14:00 </asp:ListItem>
                      <asp:ListItem> 15:00 </asp:ListItem>
                      </asp:DropDownList>
                  
              </div>

              <br />
              <br />
          <div class="registerapp">
            <asp:Button ID="registerapp" OnClick="Register_Click" runat="server" Text="Submit" />
          </div>
            </div>
        
    </form>
</body>
</html>

