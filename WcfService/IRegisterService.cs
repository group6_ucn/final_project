﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfService.Model;
using WcfService.DAL;
using System.Data;


namespace WcfService
{
    [ServiceContract]
    public interface IRegisterService
    {

        [OperationContract]
        bool SaveCustomer(Customer c);
        [OperationContract]
        bool SaveCompany(Company comp);
        [OperationContract]
        bool DeleteCustomer(string CPR);
        [OperationContract]
        bool DeleteJob(string name);
        [OperationContract]
        bool DeleteAppointment(int id);
        [OperationContract]
        bool SaveAppointment(Appointment app);
        [OperationContract]
        bool SaveJob(Job jobb);
        [OperationContract]
        bool UpdateJob(Job jobb);
        DataTable GetAllAppointmentsAsTable(string customerCPR);
        [OperationContract]
        List<Appointment> GetAllCustomersAppointmentsforUserName(string username);
        [OperationContract]
        Job findJob(int id);
        [OperationContract]
        List<Job> GetAllJobs(int companyCVR);
        [OperationContract]
        Job findJobByName(string name);
        [OperationContract]
        List<Company> GetAllCompanies();
        [OperationContract]
        Company findCompanyByName(string name);
    }

    
}
