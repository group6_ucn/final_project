﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfService.Model
{
    public class Appointment
    {
        [DataMember]
        public DateTime date { get;  set;}
        [DataMember]
        public string start_time {get; set;}
        [DataMember]
        public Job job {get;set;}
        [DataMember]
        public Customer customer { get; set; }
        [DataMember]
        public Company company { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string notes { get; set; }


    }
}