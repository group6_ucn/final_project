﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfService.Model
{
    public class Job
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int duration { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public Company company { get; set; }
    }
}