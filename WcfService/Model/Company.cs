﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfService.Model
{
    [DataContract]
    public class Company
    {
         [DataMember]
         public string name{get; set;}
         [DataMember]
         public int CVR{get; set;}
         [DataMember]
         public string owner{get; set;}
         [DataMember]
         public string Type { get; set;}


    }
}