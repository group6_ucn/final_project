﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace WcfService.Model
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string CPR { get; set; }
        [DataMember]
        public string username { get; set; }





        
    }
}