﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.Model;
using System.Data.SqlClient;
using System.Data;
using WcfService.DAL;

namespace WcfService.Controller
{
    public class CompanyCtr
    {
        private const string ConnString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\razvan\Documents\final_project\Database\Database.mdf;Integrated Security=True";
        public void InsertCompany(int C, string n, string o, string t)
        {
            DBCompany dbco = new DBCompany();
            dbco.OpenConnection(ConnString);
            dbco.InsertCompany(C, n, o, t);
            dbco.CloseConnection();

        }
        public void UpdateCompany(int CVR, string name, string owner, string type)
        {
            DBCompany dbc = new DBCompany();
            dbc.OpenConnection(ConnString);
            dbc.UpdateCompany(CVR, name, owner, type);
            dbc.CloseConnection();
        }

        public Company findCompany(int CVR)
        {
            DBCompany dbCompany = new DBCompany();
            dbCompany.OpenConnection(ConnString);
            Company m = dbCompany.findCompany(CVR);
            dbCompany.CloseConnection();
            return m;
        }

        public void DeleteCompany(int CVR)
        {
            DBCompany dbCompany = new DBCompany();
            dbCompany.OpenConnection(ConnString);
            dbCompany.DeleteCompany(CVR);
            dbCompany.CloseConnection();

        }

        public Company findCompany(string name)
        {
            DBCompany dbCompany = new DBCompany();
            dbCompany.OpenConnection(ConnString);
            Company m = dbCompany.findCompany(name);
            dbCompany.CloseConnection();
            return m;
        }

        public List<Company> GetAllCompanies()
        {
            DBCompany comp = new DBCompany();
            comp.OpenConnection(ConnString);
            List<Company> compan = comp.GetAllCompanies();
            comp.CloseConnection();
            return compan;
        }


    }
}
