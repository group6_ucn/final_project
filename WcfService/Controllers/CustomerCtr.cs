﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.Model;
using System.Data.SqlClient;
using System.Data;
using WcfService.DAL;

namespace WcfService.Controller
{
    public class CustomerCtr
    {
        private const string ConnString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\razvan\Documents\final_project\Database\Database.mdf;Integrated Security=True";
        public void InsertCustomer(string n, string e, string p, string c, string un)

        {
            DBCustomer dbc = new DBCustomer();

            dbc.OpenConnection(ConnString);
            dbc.InsertCustomer(c, n, e, p,  un);
            dbc.CloseConnection();

        }
        public void DeleteCustomer(string CPR)
        {
            DBCustomer dbCustomer = new DBCustomer();
            dbCustomer.OpenConnection(ConnString);
            dbCustomer.DeleteCustomer(CPR);
            dbCustomer.CloseConnection();

        }
        public Customer findCustomer(string CPR)
        {
            DBCustomer dbCustomer = new DBCustomer();
            dbCustomer.OpenConnection(ConnString);
            Customer m =dbCustomer.findCustomer(CPR);
            dbCustomer.CloseConnection();
            return m;
        }
        public Customer findCustomerbyUsername(string username)
        {
            DBCustomer dbCustomer = new DBCustomer();
            dbCustomer.OpenConnection(ConnString);
            Customer m = dbCustomer.findCustomerByUsername(username);
            dbCustomer.CloseConnection();
            return m;
        }
        public void UpdateCustomer(string CPR, string name, string email, string phone,  string userame)
        {
            DBCustomer dbc = new DBCustomer();
            dbc.OpenConnection(ConnString);
            dbc.UpdateCustomer(CPR, name, email, phone,  userame);
            dbc.CloseConnection();

        }


    }
}