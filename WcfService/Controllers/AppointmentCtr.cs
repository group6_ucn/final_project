﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.Model;
using WcfService.DAL;
using System.Data;

namespace WcfService.Controller
{
    public class AppointmentCtr
    {

        private const string ConnString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\razvan\Documents\final_project\Database\Database.mdf;Integrated Security=True";
        public void InsertAppointment(string start_time, int jobID, string username, int companyCVR,string notes,int id, DateTime date)
        {
            DBAppointment dba = new DBAppointment();
            dba.OpenConnection(ConnString);
            dba.insertAppointment(start_time, findJob(jobID),findCustomer(username), findCompany(companyCVR), notes,id, date);
            dba.CloseConnection();

        }
        public void deleteAppointment(int id)
        {
            DBAppointment dbc = new DBAppointment();
            dbc.OpenConnection(ConnString);
            dbc.deleteAppointment(id);
            dbc.CloseConnection();
        }
        public Job findJob(int id)
        {
            Job j = new Job();
            JobCtr dbj = new JobCtr();
            j= dbj.findJob(id);
            return j;
        }
        public Customer findCustomer(string username)
        {
            Customer c = new Customer();
            CustomerCtr cctr=new CustomerCtr();
            c = cctr.findCustomerbyUsername(username);
            return c;
        }
        public Company findCompany(int CVR)
        {
            Company comp = new Company();
            CompanyCtr dbj = new CompanyCtr();
             comp = dbj.findCompany(CVR);
            return comp;
        }
        public List<Appointment> GetAllCustomersAppointmentsforUserName(string username)
        {
            DBAppointment d = new DBAppointment();
            d.OpenConnection(ConnString);
            List<Appointment> lg = d.GetAllCustomersAppointmentsforUserName(username);
            d.CloseConnection();
            return lg;
        }
        public DataTable GetAllAppointmentsAsTable(string customerCPR)
        {
            DBAppointment d = new DBAppointment();
            d.OpenConnection(ConnString);
            DataTable lg = d.GetAllAppointmentsAsTable(customerCPR);
            d.CloseConnection();
            return lg;
        }
    }
}