﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using WcfService.Model;


namespace WcfService.DAL
{
    public class DBCustomer
    {
        private SqlConnection sqlCn = null;
        public void OpenConnection(string connectionString)
        {
            sqlCn = new SqlConnection();
            sqlCn.ConnectionString = connectionString;
            sqlCn.Open();
        }

        public void CloseConnection()
        {
            sqlCn.Close();
        }


        public void InsertCustomer(string CPR, string name, string email, string phone, string userame)
        {
            string sql = string.Format("Insert Into Customer" +
              "(CPR,name , email, phone,  username) Values" +
              "('{0}', '{1}', '{2}', '{3}', '{4}')", CPR, name, email, phone, userame);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateCustomer(string CPR, string name, string email, string phone, string username)
        {
            string sql = string.Format("Update Customer Set name = '{0}', email = '{1}', phone = '{2}',  username = '{3}' Where CPR = '{4}' ",
                name, email, phone, username, CPR);

            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }



        public void DeleteCustomer(string CPR)
        {

            string sql = string.Format("Delete from Customer where cpr = '{0}'", CPR);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        public Customer findCustomer(string CPR)
        {
            Customer c = new Customer();
            string sql = string.Format("Select name,cpr,phone,email from Customer where cpr = '{0}'", CPR);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    c = new Customer
                       {
                           name = (string)dr["Name"],
                           email = (string)dr["email"],
                           phone = (string)dr["Phone"],
                           CPR = (string)dr["CPR"],
                       };

                }
                dr.Close();
                return c;
            }


        }
        public Customer findCustomerByUsername(string username)
        {
            Customer c = new Customer();
            string sql = string.Format("Select name,cpr,phone,email,username from Customer where username = '{0}'", username);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    c = new Customer
                       {
                           name = (string)dr["Name"],
                           email = (string)dr["email"],
                           phone = (string)dr["Phone"],
                           CPR = (string)dr["CPR"],
                           username = (string)dr["username"],
                       };

                }
                dr.Close();
                return c;
            }

        }
    }
}
