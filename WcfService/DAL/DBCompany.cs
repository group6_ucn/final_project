﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WcfService.Model;
using System.Data.SqlClient;

namespace WcfService.DAL
{
    public class DBCompany
    {
        private SqlConnection sqlCn = null;
        public void OpenConnection(string connectionString)
        {
            sqlCn = new SqlConnection();
            sqlCn.ConnectionString = connectionString;
            sqlCn.Open();
        }

        public void CloseConnection()
        {
            sqlCn.Close();
        }
        public void InsertCompany(int CVR, string name, string owner, string type)
        {
            string sql = string.Format("Insert Into Company" +
              "(CVR,name , owner, type) Values" +
              "('{0}', '{1}', '{2}', '{3}')", CVR, name, owner, type);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateCompany(int CVR, string name, string owner, string type)
        {
            string sql = string.Format("Update Company Set name = '{0}', owner = '{1}', type = '{2}' Where CVR = '{3}'", 
                name, owner, type, CVR);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }


        public void DeleteCompany(int CVR)
        {

            string sql = string.Format("Delete from Company where cvr = '{0}'", CVR);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        public Company findCompany(int CVR)
        {
            Company c = new Company();
            string sql = string.Format("Select name,cvr,owner,type from Company where cvr = '{0}'", CVR);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    c = new Company
                       {
                           name = (string)dr["Name"],
                           owner = (string)dr["Owner"],
                           Type = (string)dr["Type"],
                           CVR = (int)dr["CVR"],
                       };

                }
                dr.Close();
                return c;
            }
        }

        public Company findCompany(string name)
        {
            Company c = new Company();
            string sql = string.Format("Select name,cvr,owner,type from Company where name = '{0}'", name);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    c = new Company
                    {
                        name = (string)dr["Name"],
                        owner = (string)dr["Owner"],
                        Type = (string)dr["Type"],
                        CVR = (int)dr["CVR"],
                    };

                }
                dr.Close();
                return c;
            }
        }

        public List<Company> GetAllCompanies()
        {

            List<Company> companies = new List<Company>();


            string sql = "Select * From Company";
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    Company c = new Company();
                    c.name = dr["name"].ToString();
                    c.owner = dr["owner"].ToString();
                    c.Type = dr["Type"].ToString();
                    c.CVR = Convert.ToInt32(dr["CVR"]);

                    companies.Add(c);
                }

                dr.Close();
            }
            return companies;
        }
    }
}