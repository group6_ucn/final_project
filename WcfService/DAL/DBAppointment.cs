﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.Model;
using System.Data.SqlClient;
using System.Data;

namespace WcfService.DAL
{
    public class DBAppointment
    {
        private SqlConnection sqlCn = null;
        public void OpenConnection(string connectionString)
        {
            sqlCn = new SqlConnection();
            sqlCn.ConnectionString = connectionString;
            sqlCn.Open();
        }

        public void CloseConnection()
        {
            sqlCn.Close();
        }
        public void insertAppointment(string start_time, Job jobID, Customer customer, Company companyCVR, string notes, int id, DateTime date)
        {
            string sql = string.Format("Insert Into Appointment" +
                "(id,notes, start_time, customerUsername, companyCVR, jobID, date) Values " +
                "('{0}', '{1}', '{2}', '{3}','{4}', '{5}', '{6}')", id, notes, start_time, customer.username, companyCVR.CVR, jobID.id, date);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
            }
        public List<Appointment> GetAllCustomersAppointmentsforUserName(string username)
        {

            List<Appointment> inv = new List<Appointment>();


            string sql = "Select * From Appointment where customerUsername = @customerUsername";
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.Parameters.AddWithValue("@customerUsername", username);
                SqlDataReader dr = cmd.ExecuteReader();
                
                while (dr.Read())
                {   
                    Appointment m = new Appointment();
                    Customer c = new Customer();
                    Job j = new Job();
                    m.job = j;                   
                    j.id = Convert.ToInt32(dr["jobid"]);
                    m.customer = c;
                    m.id = Convert.ToInt32(dr["id"]);
                    m.notes = dr["notes"].ToString();
                    m.date = Convert.ToDateTime(dr["date"]);
                    m.start_time = dr["start_time"].ToString();
                    inv.Add(m);
                }

                dr.Close();
            }
            return inv;
        }

        public DataTable GetAllAppointmentsAsTable(string customerCPR)
        {
            DataTable inv = new DataTable();


            string sql = string.Format("Select * From Appointment where customerCPR = @customerCPR");

            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.Parameters.AddWithValue("@customerCPR", customerCPR);
                SqlDataReader dr = cmd.ExecuteReader();
                inv.Load(dr);
                dr.Close();
            }
            return inv;
        }
        public void deleteAppointment(int id)
        {
            string sql = string.Format("Delete from Appointments where id = '{0}'", id);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        }
    }


