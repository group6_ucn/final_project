﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WcfService.Model;
using System.Data.SqlClient;

namespace WcfService.DAL
{
    public class DBJob
    {
        private SqlConnection sqlCn = null;

        public void OpenConnection(string connectionString)
        {
            sqlCn = new SqlConnection();
            sqlCn.ConnectionString = connectionString;
            sqlCn.Open();
        }

        public void CloseConnection()
        {
            sqlCn.Close();
        }

<<<<<<< HEAD
        public void InsertJob(int id, int duration, string description,string name)
        {
            string sql = string.Format("Insert Into Job" + "(id, duration, description, name) Values" +
                "('{0}', '{1}', '{2}', '{3}')", id, duration, description, name);
=======
        public void InsertJob(string name, int duration, string description, Company companyCVR)
        {
            string sql = string.Format("Insert Into Job" + "(name, duration, description, companyCVR) Values" +
                "('{0}', '{1}', '{2}','{3}')", name, duration, description, companyCVR.CVR);
>>>>>>> 91128cc1043519b4535d6676e303051d254aa332
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }

<<<<<<< HEAD
        public void UpdateJob(int id, int duration, string description)
        {
            string sql = string.Format("Update Job Set duration ='{0}', description = '{1}' Where id = '{2}'",
                 duration, description, id);
=======
        public void UpdateJob(string name, int duration, string description, Company companyCVR)
        {
            string sql = string.Format("Update Job Set name = '{0}', duration ='{1}', description = '{2}', companyCVR = '{3}' Where name = '{0}'",
                name, description, duration, companyCVR);
>>>>>>> 91128cc1043519b4535d6676e303051d254aa332
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        public void DeleteJob(string name)
        {
            string sql = string.Format("Delete from Job where name = '{0}'", name);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        public Job findJob(int id)
        {
            Job j = new Job();
            string sql = string.Format("Select name,description,duration,id from Job where id= '{0}'", id);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    j = new Job
                       {
                           name = (string)dr["Name"],
                           duration = (int)dr["Duration"],
                           description = (string)dr["Description"],
                           id = (int)dr["id"],

                       };

                }
                dr.Close();
                return j;
            }
        }
        public List<Job> GetAllJobs(int companyCVR)
        {

            List<Job> jobs = new List<Job>();


            string sql = "Select * From Job where companyCVR = @companyCVR ";
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                cmd.Parameters.AddWithValue("@companyCVR", companyCVR);
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    Job j = new Job();
                    Company c = new Company();
                    j.id = Convert.ToInt32(dr["id"]);
                    j.name = dr["name"].ToString();
                    j.description = dr["description"].ToString();
                    j.company = c;
                    c.CVR = Convert.ToInt32(dr["companyCVR"]);
                    
                    jobs.Add(j);
                }

                dr.Close();
            }
            return jobs;
        }
        public Job findJobByName(string name)
        {
            Job j = new Job();
            string sql = string.Format("Select name,description,duration,id from Job where name= '{0}'", name);
            using (SqlCommand cmd = new SqlCommand(sql, this.sqlCn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    j = new Job
                    {
                        name = (string)dr["Name"],
                        duration = (int)dr["Duration"],
                        description = (string)dr["Description"],
                        id = (int)dr["id"],
                    };

                }
                dr.Close();
                return j;
            }
        }
    }
}