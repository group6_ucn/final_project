﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using WcfService.Model;
using System.Text;
using System.ServiceModel;
using WcfService.DAL;
using WcfService.Controller;
using System.Data;

namespace WcfService
{
    public class RegisterService : IRegisterService
    {
        CustomerCtr ctr = new CustomerCtr();
        CompanyCtr compctr = new CompanyCtr();
        AppointmentCtr appctr = new AppointmentCtr();
        JobCtr jobctr = new JobCtr();


        public bool SaveCustomer(Customer c)
        {
            ctr.InsertCustomer(c.name, c.email, c.phone, c.CPR, c.username);
            return true;
        }
        public bool SaveCompany(Company comp)
        {
            compctr.InsertCompany(comp.CVR, comp.name, comp.owner, comp.Type);
            return true;
        }
        public bool DeleteCustomer(string CPR)
        {
            ctr.DeleteCustomer(CPR);
            return true;
        }
        public bool DeleteJob(string name)
        {
            jobctr.DeleteJob(name);
            return true;
        }
        public bool DeleteAppointment(int id)
        {
                appctr.deleteAppointment(id);
                return true;
        }
        public bool SaveAppointment(Appointment app)
        {
            appctr.InsertAppointment(app.start_time, app.job.id, app.customer.username, app.company.CVR, app.notes, app.id, app.date);
            return true;
        }

        public DataTable GetAllAppointmentsAsTable(string customerCPR)
        {
            DataTable data = appctr.GetAllAppointmentsAsTable(customerCPR);
            return data;
        }

        public List<Appointment> GetAllCustomersAppointmentsforUserName(string username)
        {
            List<Appointment> list = appctr.GetAllCustomersAppointmentsforUserName(username);
            return list;
        }

        public Job findJob(int id)
        {
            JobCtr jobctr = new JobCtr();
            Job j = jobctr.findJob(id);
            return j;
        }
<<<<<<< HEAD
        public bool UpdateJob(Job jobb)
        {
            jobctr.UpdateJob(jobb.duration, jobb.description, jobb.id);
                return true;
        }
        public bool SaveJob(Job jobb)
        {
            jobctr.InsertJob(jobb.id, jobb.duration , jobb.description, jobb.name);
            return true;
        }

        public List<Job> GetAllJobs() 
=======
        public List<Job> GetAllJobs(int companyCVR) 
>>>>>>> 91128cc1043519b4535d6676e303051d254aa332
        {
            List<Job> jobs = jobctr.GetAllJobs(companyCVR);
            return jobs;
        }

        public List<Company> GetAllCompanies()
        {
            List<Company> companies = compctr.GetAllCompanies();
                return companies;
        }
        public Job findJobByName(string name)
        {
            JobCtr jobctr = new JobCtr();
            Job j = jobctr.findJobByName(name);
            return j;
        }

        public Company findCompanyByName(string name)
        {
            CompanyCtr compctr = new CompanyCtr();
            Company c = compctr.findCompany(name);
            return c;
        }
    }
}